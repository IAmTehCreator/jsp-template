<%@ page session="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title></title>

		<link rel="author" href="https://bitbucket.org/IAmTehCreator/" />
		<link rel="stylesheet" type="text/css" href="resources/styles.css" />

		<script type="text/javascript" src="resources/script.js"></script>
	</head>

	<body>
		<h1><% out.println("Hello world!"); %></h1>
	</body>
</html>
