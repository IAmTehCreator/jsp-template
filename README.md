# JSP Web App Boilerplate
This repository contains boilerplate code which implements a simple JSP/Servlet web app with the required
build tools. This boilerplate code assumes [Apache Tomcat](http://tomcat.apache.org/) is the server software.

## Building
This project uses [Apache Ant](http://ant.apache.org/) as a build system and has multiple ANT targets
preconfigured to build the app. Before you can use any of the build targets you must provide the
`tomcat` property in the build.properties file, this must point to the root of the Apache Tomcat
installation, this is required to create the classpath for the build.

It is recommended that you customise the build.properties and local.build.properties files for your build
environment before you do anything.

Below is a list of the preconfigured targets:

* `clean` - Delete old build and dist directories
* `prepare` - Create the build destination directory and copy static resources
* `compile` - Compile the Java source code
* `dist` - Create binary distribution
* `javadoc` - Create JavaDoc API documentation from Java source code

The Ant project also contains targets for interacting with the Tomcat manager allowing automatic
deploys and undeploys. For these targets to work you must be running Tomcat locally and you must
also provide a username and password in the `build.properties` file for a user which has the
`manager-script` role.

The deployment targets are:

* `list` - List all of the apps installed in the servlet container
* `install` - Install the app onto the servlet container
* `restart` - Restart the app on the servlet container
* `uninstall` - Uninstall the app from the servlet container
