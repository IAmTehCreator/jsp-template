package com.smilne;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class Servlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * Respond to a GET request for the content produced by this servlet.
	 *
	 * @param request The servlet request we are processing
	 * @param response The servlet response we are producing
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet error occurs
	 */
	@Override
	public void doGet ( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * Respond to a POST request for the content produced by this servlet.
	 *
	 * @param request The servlet request we are processing
	 * @param response The servlet response we are producing
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet error occurs
	 */
	@Override
	public void doPost ( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}


}
